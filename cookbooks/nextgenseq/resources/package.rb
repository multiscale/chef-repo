actions :install

attribute :app,          :kind_of => String, :name_attribute => true
attribute :source,       :kind_of => String, :required => true
attribute :destination,  :kind_of => String
attribute :binaries,     :kind_of => Array, :default => Array.new
attribute :wrapper,      :kind_of => String
attribute :extract_command, :kind_of => String

def initialize(name, run_context = nil)
  super
  @action = :install
end
