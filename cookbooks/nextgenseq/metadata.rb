maintainer       "Michael Linderman"
maintainer_email "michael.linderman@mssm.edu"
license          "MIT"
description      "Installs/configures next generation sequencing analysis tools"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.rdoc'))
version          "0.0.1"

recipe "prereq", "Setups pre-requisites for installing next generation tools" 

recipe "gatk", "Installs GATK"
recipe "bwa", "Installs bwa"
recipe "samtool", "Installs SAMTools"

%w{ debian ubuntu }.each do |os|
  supports os
end
