module Archive
	
	def extension(path=archive_path)
		case path
		when /\.tar\.bz2\z/
			return ".tar.bz2"
		when /\.tar\.gz\z/
			return ".tar.gz"
		else
			return ::File.extname(path)
		end
	end

	def extract_command(path=archive_path)
		if defined?(new_resource) and !new_resource.extract_command.nil?
			new_resource.extract_command
		else
			case path
			when /\.tar\.bz2\z/
				"tar -C #{node[:nextgenseq][:system_dir]} -xjf #{path}"
			when /\.tar\.gz\z/
				"tar -C #{node[:nextgenseq][:system_dir]} -xzf #{path}"
			when /\.tgz\z/
				"tar -C #{node[:nextgenseq][:system_dir]} -xzf #{path}"
			when /\.zip\z/
				"unzip -u -d #{node[:nextgenseq][:system_dir]} #{path}"
			else
				"mkdir -p #{extract_path} && cp #{path} #{extract_path}/."
			end
		end
	end

	def archive_path(source=new_resource.source)
		::File.join(Chef::Config[:file_cache_path],  ::File.basename(source))
	end

	def extract_path(source=nil)
		if defined?(new_resource) and !new_resource.destination.nil?
			::File.join(node[:nextgenseq][:system_dir], new_resource.destination)
		else 
			source ||= new_resource.source
			::File.join(node[:nextgenseq][:system_dir], ::File.basename(source, extension(source)))
		end
	end
	
end
