include Archive

action :install do
	Chef::Log.info "Installing package #{new_resource.name}"

	package "Install unzip for #{new_resource.name}" do
		package_name "unzip"
		action :install
		only_if do
			extension == ".zip"	
		end
	end

	execute "download #{new_resource.name}" do
		command "wget -O #{archive_path} #{new_resource.source}"
		not_if do
			::File.exists?(archive_path)
		end
	end

	execute "unpack #{new_resource.name}" do
		command extract_command 
		not_if "test -d #{extract_path}"
	end

	template "/etc/profile.d/#{new_resource.name}.sh" do
		source "profile_d.erb"
		variables(:envvars => { "#{new_resource.name.upcase}_HOME" => extract_path })
		owner "root"
		group "root"
		mode  "0755"
	end

	if new_resource.wrapper
		template "#{node[:nextgenseq][:binary_dir]}/#{new_resource.name}" do
			source new_resource.wrapper 
			variables(:extract_path => extract_path)
			owner "root"
			group "root"
			mode  "0755"
		end
	end

end
