include Archive

action :install do
	Chef::Log.info "Installing package #{new_resource.name}"

	package "Install unzip for #{new_resource.name}" do
		package_name "unzip"
		action :install
		only_if do
			extension == ".zip"	
		end
	end

	execute "download #{new_resource.name}" do
		command "wget -O #{archive_path} #{new_resource.source}"
		not_if do
			::File.exists?(archive_path)
		end
	end

	execute "unpack #{new_resource.name}" do
		command extract_command 
		not_if "test -d #{extract_path}"
	end

	unless new_resource.binaries.empty?
		execute "install #{new_resource.name}" do
			command "cp -u -t #{node[:nextgenseq][:binary_dir]} #{new_resource.binaries.join(' ')}"
			cwd extract_path
		end
	end
end
