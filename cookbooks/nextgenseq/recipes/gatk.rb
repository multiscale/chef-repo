#
# Cookbook Name:: nextgenseq
# Recipe:: gatk
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"

nextgenseq_package "gatk" do
	source "ftp://ftp.broadinstitute.org/pub/gsa/GenomeAnalysisTK/GenomeAnalysisTK-#{node[:nextgenseq][:gatk][:version]}.tar.bz2"
	wrapper "gatk.erb"
	action :install
	provider :nextgenseq_java
end

nextgenseq_package "queue" do
	source "ftp://ftp.broadinstitute.org/pub/gsa/Queue/Queue-#{node[:nextgenseq][:queue][:version]}.tar.bz2"
	wrapper "queue.erb"
	action :install
	provider :nextgenseq_java
end
