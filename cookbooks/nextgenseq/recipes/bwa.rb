#
# Cookbook Name:: nextgenseq
# Recipe:: bwa
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"
include_recipe "build-essential"

nextgenseq_package "bwa" do
	source "http://downloads.sourceforge.net/project/bio-bwa/bwa-#{node[:nextgenseq][:bwa][:version]}.tar.bz2"
	binaries %w{ bwa solid2fastq.pl qualfa2fq.pl }
	action :install
	provider :nextgenseq_make_and_copy
end
