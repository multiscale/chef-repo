# Recipe:: cufflinks
#
# Copyright 2012, Michael Linderman
#

if node[:languages][:ruby][:host_cpu] != "x86_64"
	raise Chef::Exceptions::UnsupportedAction, "tophat only supported on x86_64"
end

include_recipe "nextgenseq::prereq"

version = node[:nextgenseq][:cufflinks][:version]

nextgenseq_package "cufflinks" do
	source "http://cufflinks.cbcb.umd.edu/downloads/cufflinks-#{version}.Linux_x86_64.tar.gz"
	binaries %w{ cufflinks cuffdiff cuffcompare }
	action :install
	provider :nextgenseq_copy
end	
