# Recipe:: minimac
#
# Copyright 2012, Michael Linderman
#

class Chef::Recipe
  include Archive 
end

include_recipe "nextgenseq::prereq"

if node[:languages][:ruby][:host_cpu] != "x86_64"
	raise Chef::Exceptions::UnsupportedAction, "minimac only supported on x86_64"
end

# Minimac is packaged without directory, so we need to override default extraction
minimac = "minimac-#{node[:nextgenseq][:minimac][:version]}.tgz"
archive = archive_path(minimac)
extract = extract_path(minimac)

nextgenseq_package "minimac" do
	source "http://www.sph.umich.edu/csg/cfuchsb/#{minimac}"
	extract_command "mkdir -p #{extract} && tar -C #{extract} -xzf #{archive}"
	binaries %w{ minimac minimac-omp }
	action :install
	provider :nextgenseq_copy
end
