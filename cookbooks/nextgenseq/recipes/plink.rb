#
# Cookbook Name:: nextgenseq
# Recipe:: bwa
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"

nextgenseq_package "plink" do
	source "http://pngu.mgh.harvard.edu/~purcell/plink/dist/plink-#{node[:nextgenseq][:plink][:version]}-#{node[:languages][:ruby][:host_cpu]}.zip"
	binaries %w{ plink }
	action :install
	provider :nextgenseq_copy
end

version_pseq = node[:nextgenseq][:plinkseq][:version]

nextgenseq_package "plinkseq" do
	source "http://atgu.mgh.harvard.edu/plinkseq/dist/version-#{version_pseq}/plinkseq-#{version_pseq}-#{node[:languages][:ruby][:host_cpu]}.tar.gz"
	binaries %w{ pseq }
	action :install
	provider :nextgenseq_copy
end
