#
# Cookbook Name:: nextgenseq
# Recipe:: default
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"

# Install all of the tools
%w{ beagle bowtie bwa cufflinks gatk minimac picard plink samtools tophat soap }.each do |r|
	include_recipe "nextgenseq::#{r}"
end
