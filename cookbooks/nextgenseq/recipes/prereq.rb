#
# Cookbook Name:: nextgenseq
# Recipe:: prereq
#
# Copyright 2012, Michael Linderman
#

case node['platform']
when "ubuntu", "debian"
	include_recipe "apt"
end

[node[:nextgenseq][:system_dir], node[:nextgenseq][:binary_dir]].each do |d|
	directory d do
		owner "root"
		group "root"
		mode  "0755"
		action :create
	end
end
