#
# Cookbook Name:: nextgenseq
# Recipe:: samtools
#
# Copyright 2012, Michael Linderman
#

class Chef::Recipe
  include Archive 
end

include_recipe "nextgenseq::prereq"
include_recipe "build-essential"

case node['platform']
when "ubuntu", "debian"
	%w{ libncurses5 libncurses5-dev }.each do |p|
		package p do
			action :install
		end
	end
when "centos","redhat","fedora"	
	%w{ ncurses ncurses-devel }.each do |p|
		package p do
			action :install
		end
	end
end

version = node[:nextgenseq][:samtools][:version]

extract = extract_path("samtools-#{version}.tar.bz2")

nextgenseq_package "samtools" do
	source "http://downloads.sourceforge.net/project/samtools/samtools/#{version}/samtools-#{version}.tar.bz2"
	binaries %w{ samtools misc/samtools.pl }
	action :install
	provider :nextgenseq_make_and_copy
end

# Install libbam and headers for use by other tools, e.g. tophat

execute "install samtools library" do
	command "cp -u -t #{node[:nextgenseq][:lib_dir]} #{::File.join(extract,"libbam.a")}"
end
 
include_path = ::File.join(node[:nextgenseq][:header_dir], "bam")

directory include_path do
	owner "root"
	group "root"
	action :create
end

execute "install samtools headers" do
	command "cp -u -t #{include_path} #{::File.join(extract,"*.h")}"
end
