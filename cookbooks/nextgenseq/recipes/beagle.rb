#
# Cookbook Name:: nextgenseq
# Recipe:: beagle
#
# Copyright 2012, Michael Linderman
#

class Chef::Recipe
  include Archive 
end

include_recipe "nextgenseq::prereq"

nextgenseq_package "beagle" do
	source "http://faculty.washington.edu/browning/beagle/beagle.jar"
	wrapper "beagle.erb"
	action :install
	provider :nextgenseq_java
end

%w{beagle2linkage linkage2beagle transpose }.each do |jar|
	archive = archive_path("#{jar}.jar")
	extract = extract_path("beagle.jar")
	nextgenseq_package jar do
		source "http://faculty.washington.edu/browning/beagle_utilities/#{jar}.jar"
		extract_command "cp -u -t #{extract} #{archive}"
		action :install
		provider :nextgenseq_copy
	end
end
