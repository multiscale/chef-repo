#
# Cookbook Name:: nextgenseq
# Recipe:: picard
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"

version = node[:nextgenseq][:picard][:version]

nextgenseq_package "picard" do
	source "http://downloads.sourceforge.net/project/picard/picard-tools/#{version}/picard-tools-#{version}.zip"
	wrapper "picard.erb"
	action :install
	provider :nextgenseq_java
end
