#
# Cookbook Name:: nextgenseq
# Recipe:: soap
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"

version = node[:nextgenseq][:soap][:version] 

nextgenseq_package "soap" do
	source "http://soap.genomics.org.cn/down/soap#{version}release.tar.gz"
	binaries %w{ soap 2bwt-builder }
	action :install
	provider :nextgenseq_copy
end
