#
# Cookbook Name:: bowtie
# Recipe:: bwa
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"
include_recipe "build-essential"

version = node[:nextgenseq][:bowtie][:version]

nextgenseq_package "bowtie" do
	source "http://downloads.sourceforge.net/project/bowtie-bio/bowtie/#{version}/bowtie-#{version}-src.zip"
	destination "bowtie-#{version}"
	binaries %w{ bowtie bowtie-build bowtie-inspect }
	action :install
	provider :nextgenseq_make_and_copy
end
