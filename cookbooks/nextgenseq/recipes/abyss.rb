#
# Cookbook Name:: nextgenseq
# Recipe:: abyss
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"
include_recipe "build-essential"

version = node[:nextgenseq][:abyss][:version]

%w{ libboost-dev sparsehash }.each do |p|
	package p do
		action :install
	end
end

nextgenseq_package "abyss" do
	source "http://www.bcgsc.ca/downloads/abyss/abyss-#{version}.tar.gz" 
	action :install
	binaries %w{ bin/abyss-adjtodot.pl bin/abyss-bowtie2 bin/abyss-bwasw bin/abyss-fac.pl bin/abyss-kaligner bin/abyss-samtoafg bin/abyss-bowtie bin/abyss-bwa bin/abyss-cstont bin/abyss-joindist bin/abyss-pe ABYSS/ABYSS}
	provider :nextgenseq_configure_make_and_copy
end
