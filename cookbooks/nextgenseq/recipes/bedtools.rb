#
# Cookbook Name:: nextgenseq
# Recipe:: bedtools
#
# Copyright 2012, Michael Linderman
#

include_recipe "nextgenseq::prereq"
include_recipe "build-essential"

version = node[:nextgenseq][:bedtools][:version]

nextgenseq_package "bedtools" do
	source "http://bedtools.googlecode.com/files/BEDTools.v#{version}.tar.gz"
	action :install
	destination "BEDTools-Version-#{version}"
	binaries %w{ bin/annotateBed bin/bed12ToBed6 bin/bedToIgv bin/clusterBed bin/expandCols bin/genomeCoverageBed bin/intersectBed bin/maskFastaFromBed bin/multiIntersectBed bin/pairToPair bin/slopBed bin/tagBam bin/windowMaker bin/bamToBed bin/bedpeToBam bin/bedtools bin/complementBed bin/fastaFromBed bin/getOverlap bin/linksBed bin/mergeBed bin/nucBed bin/randomBed bin/sortBed bin/unionBedGraphs bin/bamToFastq bin/bedToBam bin/closestBed bin/coverageBed bin/flankBed bin/groupBy bin/mapBed bin/multiBamCov bin/pairToBed bin/shuffleBed bin/subtractBed bin/windowBed }
	provider :nextgenseq_make_and_copy
end
