# Recipe:: tophat
#
# Copyright 2012, Michael Linderman
#

if node[:languages][:ruby][:host_cpu] != "x86_64"
	raise Chef::Exceptions::UnsupportedAction, "tophat only supported on x86_64"
end

include_recipe "nextgenseq::prereq"
include_recipe "nextgenseq::samtools"
include_recipe "nextgenseq::bowtie"

version = node[:nextgenseq][:tophat][:version]

nextgenseq_package "tophat" do
	source "http://tophat.cbcb.umd.edu/downloads/tophat-#{version}.Linux_x86_64.tar.gz"
	binaries %w{ bam2fastx bam_merge bed_to_juncs closure_juncs contig_to_chr_coords extract_reads fix_map_ordering gtf_juncs gtf_to_fasta juncs_db library_stats long_spanning_reads map2gtf mask_sam prep_reads sam_juncs segment_juncs sra_to_solid tophat tophat_reports wiggles }
	action :install
	provider :nextgenseq_copy
end
