maintainer       "Michael Linderman"
maintainer_email "michael.linderman@mssm.edu"
license          ""
description      "Installs/Configures komonzu"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.rdoc'))
version          "0.0.1"
