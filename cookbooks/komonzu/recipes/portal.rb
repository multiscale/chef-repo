include_recipe "apt"

if node.platform == "ubuntu"

	# Temporarily and permanently change the locale to UTF8
	file "/etc/default/locale" do
		owner "root"
		group "root"
		mode  "0644"
		content 'LANG="en_US.utf8"'
		action :create
	end
	
	ENV["LANG"]    = "en_US.utf8" if ENV["LANG"]    != "en_US.utf8" 
	ENV["LC_TYPE"] = "en_US.utf8" if ENV["LC_TYPE"] != "en_US.utf8" 

else
	raise Chef::Exceptions::UnsupportedAction, 'Platform not yet supported'
end


name = "komonzu"

user name do
	comment "#{name} application user"
	home "/srv/#{name}"
	shell "/bin/bash"
end

node['apache']['listen_ports'] << "443" unless node['apache']['listen_ports'].include?("443")

include_recipe "apache2"
include_recipe "apache2::mod_ssl"
include_recipe "apache2::mod_rewrite"
include_recipe "apache2::mod_headers"
include_recipe "apache2::mod_expires"
include_recipe "apache2::mod_deflate"
include_recipe "passenger_apache2::mod_rails"

directory "/etc/chef/certificates" do
  mode "700"
end

bash "Create SSL Certificates" do
  cwd "/etc/chef/certificates"
  code <<-EOH
  umask 077
  openssl genrsa 2048 > #{name}.key
  openssl req -subj "#{node['application']['ssl_req']}" -new -x509 -nodes -sha1 -days 3650 -key #{name}.key > #{name}.crt
  cat #{name}.key #{name}.crt > #{name}.pem
  EOH
  not_if { ::File.exists?("/etc/chef/certificates/#{name}.pem") }
end


hostname = node[:ec2] ? node[:ec2][:public_hostname] : node[:fqdn]

directory "/srv/#{name}" do
  owner name
  group name
  mode "0755"
  action :create
end

web_app name do
  docroot "/srv/#{name}/current/public"
  server_name hostname 
	server_aliases [ name, hostname ]
	log_dir node[:apache][:log_dir]
	user  name
	group name
end

apache_site "000-default" do
  enable false
end

include_recipe "postgresql::server"

if node.platform == "ubuntu"

	# Install additional packages needed by 'pg' gem
	%w{ postgresql-contrib libpq-dev }.each do |p|
		package p
	end

else
	raise Chef::Exceptions::UnsupportedAction, 'Platform not yet supported'
end

execute "create-database-user" do
  command "createuser -SDRw #{name}"
  only_if "psql -c \"SELECT COUNT(*) FROM pg_user WHERE usename='#{name}'\" | sed -n 3p | grep 0", :user => "postgres"
	user "postgres"
end


["#{name}_test", "#{name}_development", "#{name}_production"].each do |d|
	execute "create-database-#{d}" do
		command "createdb -O #{name} -l en_US.utf8 -E utf8 -T template0 #{d}"
		not_if "psql -l | grep #{d}", :user => "postgres"
		user "postgres"
	end
end
