#
# Cookbook Name:: komonzu
# Recipe:: sandbox
#
# Copyright 2011, Mount Sinai School of Medicine
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Create the sandbox user
sandbox_user = node[:sandbox][:user]

user sandbox_user[:handle] do
	comment sandbox_user[:comment]
	home "/home/#{sandbox_user[:handle]}"
	shell "/bin/bash"
	supports :manage_home => :true
end

# Fill the authorized keys with their SSH key
directory "/home/#{sandbox_user[:handle]}/.ssh" do
	owner sandbox_user[:handle]
	group sandbox_user[:handle]
	mode 0700
end

template "/home/#{sandbox_user[:handle]}/.ssh/authorized_keys" do
	source "authorized_keys.erb"
	owner sandbox_user[:handle]
	group sandbox_user[:handle]
	mode 0600
	variables :ssh_keys => sandbox_user[:ssh_keys]
end

# Load in the environment variables
template "/home/#{sandbox_user[:handle]}/.bash_profile" do
	source "bash_profile.erb"
	owner sandbox_user[:handle]
	group sandbox_user[:handle]
	mode 0600
	variables :environ => node[:sandbox][:environ]
end

# Install Komonzu gem
gem_package "komonzu" do
	action :install
end
